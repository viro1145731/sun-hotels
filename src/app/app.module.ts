import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // Import ReactiveFormsModule


import { AppComponent } from './app.component';
import { HotelListComponent } from './component/hotel-list/hotel-list.component';

// Import the FontAwesomeModule
import {  FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RoomListComponent } from './component/room-list/room-list.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'rooms/:id', component: RoomListComponent },
  { path: 'hotel', component: HotelListComponent }

];

@NgModule({
  declarations: [
    AppComponent,
    HotelListComponent,
    RoomListComponent
    
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
