import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { map, startWith, catchError, of, BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { DataState } from 'src/app/enum/data-state.enum';
import { AppState } from 'src/app/interface/app-state';
import { CustomResponse } from 'src/app/interface/custom-response';
import { Hotel } from 'src/app/interface/hotel';
import { HotelService } from 'src/app/service/hotel.service';

// Importing the necessary Font Awesome modules
import { faCheckCircle, faTimesCircle, faSpinner, faMagnifyingGlass, faCoffee, faFilter, faAdd, faHotel, faCircleXmark, faXmark, faTrash, faPen, faShare, faEye, faPrint } from '@fortawesome/free-solid-svg-icons';
import { __values } from 'tslib';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';


@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css'],
})


export class HotelListComponent implements OnInit {
  
  hotelForm: FormGroup;

  appState$: Observable<AppState<CustomResponse>>;
  readonly DataState = DataState;
  private dataSubject = new BehaviorSubject<CustomResponse>(null);
  
  //Loading to save part
  private isLoading = new BehaviorSubject<Boolean>(false);
  isLoading$ = this.isLoading.asObservable();

  //To Filter Option
  @ViewChild('teams') teams!: ElementRef;
  selectedOption: string = '';
  onOptionChange(): void {
    this.selectedOption= this.teams.nativeElement.value;
  }


  //Model using to add Hotels
  
  openModal() {
    const modelDiv = document.getElementById('addHotelModel');
    if(modelDiv != null){
      modelDiv.style.display = 'block';
    }
    //this.modalVisible = true;
  }
  closeModal() {
    const modelDiv = document.getElementById('addHotelModel');
    if(modelDiv != null){
      modelDiv.style.display = 'none';
    }
    //this.modalVisible = false;
  }


  //To use in Date inputs
  isStartDate: boolean = true;
  
  //To Search
  searchTerm: string = '';



  // Defining the Font Awesome icons
  faCheckCircle = faCheckCircle;
  faTimesCircle = faTimesCircle;
  faCoffee = faCoffee;
  faFilter = faFilter;
  faAdd = faAdd;
  faHotel = faHotel;
  faSearch = faMagnifyingGlass;
  faSpinner = faSpinner;
  faCross = faXmark;
  faTrash = faTrash;
  faPen = faPen;
  faShare = faEye;
  faPrint = faPrint;

  constructor(
    private formBuilder: FormBuilder,
    private hotelService: HotelService
    ) {
      this.hotelForm = this.formBuilder.group({
        hotelName: ['', Validators.required],
        location: ['', Validators.required],
        startDate: ['', Validators.required],
        endDate: ['', Validators.required],
        status: [true]
      });

    }

  checkStatus(endDate: Date, startDate: Date): any {
    if (new Date(endDate) >= new Date() && new Date(startDate) <= new Date() || new Date(endDate) == new Date()) {
      return true;
    } else{
      return false;
    }
  }

  printPage(): void {
    window.print();
  }

  ngOnInit(): void {
    this.appState$ = this.hotelService.hotels$
    .pipe(
      map(response => {
        this.dataSubject.next(response);
        return { dataState: DataState.LOADED_STATE, appData: { ...response, data:{ hotels: response.data.hotels.reverse() } } }
      }),
      startWith({ dataState: DataState.LOADING_STATE }),
      catchError((error: string) => {
        return of({ dataState: DataState.ERROR_STATE, error  })
      })
    );
  }

  

  saveHotel(hotelForm: NgForm): void {
    this.isLoading.next(true);
    this.appState$ = this.hotelService.save$(hotelForm.value as Hotel)
    .pipe(
      map(response => {
        this.dataSubject.next(
          { ...response, data: { hotels: [response.data.hotel, ...this.dataSubject.value.data.hotels]} }
        );
        //document.getElementById('closeModel').click();
        this.closeModal();
        this.isLoading.next(false);
        hotelForm.resetForm();
        return { dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }
      }),
      startWith({ dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }),
      catchError((error: string) => {
        this.isLoading.next(false);
        return of({ dataState: DataState.ERROR_STATE, error  })
      })
    );
  }

  searchHotels(searchTerm: string): void {
    this.appState$ = this.hotelService.search$(searchTerm, this.dataSubject.value)
      .pipe(
        map(response => {
          return { dataState: DataState.LOADED_STATE, appData: response }
        }),
        startWith({ dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }),
        catchError((error: string) => {
          return of({ dataState: DataState.ERROR_STATE, error  })
        })
      )
  }

  filterHotels(status: string): void {
    this.appState$ = this.hotelService.filter$(status, this.dataSubject.value)
    .pipe(
      map(response => {
        return { dataState: DataState.LOADED_STATE, appData: response }
      }),
      startWith({ dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }),
      catchError((error: string) => {
        return of({ dataState: DataState.ERROR_STATE, error  })
      })
    );
  }


  deleteHotel(hotel: Hotel): void {
    this.appState$ = this.hotelService.delete$( hotel.id )
    .pipe(
      map(response => {
        this.dataSubject.next(
          { ...response, data: 
            { hotels: this.dataSubject.value.data.hotels.filter(h => h.id !== hotel.id)}
          }
        );
        return { dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }
      }),
      startWith({ dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }),
      catchError((error: string) => {
        return of({ dataState: DataState.ERROR_STATE, error  })
      })
    );
  }
}
