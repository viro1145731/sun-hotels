import { Component,ViewChild, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { BehaviorSubject, Observable, catchError, map, of, startWith } from "rxjs";
import { DataState } from "src/app/enum/data-state.enum";
import { AppState } from "src/app/interface/app-state";
import { CustomResponse } from "src/app/interface/custom-response";
import { HotelService } from "src/app/service/hotel.service";
import { RoomService } from "src/app/service/room.service";

// Importing the necessary Font Awesome modules
import { faCheckCircle, faTimesCircle, faSpinner, faMagnifyingGlass, faCoffee, faFilter, faAdd, faHotel, faCircleXmark, faXmark, faTrash, faPen, faShare, faEye, faPrint } from '@fortawesome/free-solid-svg-icons';
import { __values } from 'tslib';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Room } from "src/app/interface/room";

@Component({
    selector: 'app-room-list',
    templateUrl: './room-list.component.html',
    styleUrls: ['./room-list.component.css']
})

export class RoomListComponent implements OnInit {

    hotelId: number;
    hotelData: boolean = false;
    hotel: Observable<CustomResponse>;
    //hotel$: Observable<AppState<CustomResponse>>;
    //private hotelSubject = new BehaviorSubject<CustomResponse>(null);

    roomForm: FormGroup;

    appState$: Observable<AppState<CustomResponse>>;
    readonly DataState = DataState;

    private dataSubject = new BehaviorSubject<CustomResponse>(null);

    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private roomService: RoomService,
        private hotelService: HotelService
    ){
      this.roomForm = this.formBuilder.group({
        roomType: ['', Validators.required],
        roomNumber: ['', Validators.required],
        maxAdult: ['', Validators.required],
        price: ['', Validators.required],
        hotel: this.hotel
      })

    }

    //Loading to save part
    private isLoading = new BehaviorSubject<Boolean>(false);
    isLoading$ = this.isLoading.asObservable();

 
    //To Search
    searchTerm: string = '';


    // Defining the Font Awesome icons
    faCheckCircle = faCheckCircle;
    faTimesCircle = faTimesCircle;
    faCoffee = faCoffee;
    faFilter = faFilter;
    faAdd = faAdd;
    faHotel = faHotel;
    faSearch = faMagnifyingGlass;
    faSpinner = faSpinner;
    faCross = faXmark;
    faTrash = faTrash;
    faPen = faPen;
    faShare = faEye;
    faPrint = faPrint;

    printPage(): void {
      window.print();
    }
    checkStatus(endDate: Date, startDate: Date): any {
      if (new Date(endDate) >= new Date() && new Date(startDate) <= new Date() || new Date(endDate) == new Date()) {
        return true;
      } else{
        return false;
      }
    }

    ngOnInit(): void {

        this.route.paramMap.subscribe(params => {
            this.hotelId = +params.get('id');
            
            this.appState$ = this.roomService.rooms$(this.hotelId).pipe(
                map(response => {
                  this.dataSubject.next(response);
                  return { dataState: DataState.LOADED_STATE, appData: { ...response, data:{ rooms: response.data.rooms.reverse() } } }
                }),
                startWith({ dataState: DataState.LOADING_STATE }),
                catchError((error: string) => {
                  return of({ dataState: DataState.ERROR_STATE, error  })
                })
              );

            
            this.hotel = this.hotelService.hotel$(this.hotelId)
            if(this.hotel != null){
              this.hotelData=true;
            }
          });
        
    }

    saveRoom(roomForm: NgForm, hotelId: number): void {
      this.isLoading.next(true);
      this.appState$ = this.roomService.save$(roomForm.value as Room, hotelId)
      .pipe(
        map(response => {
          this.dataSubject.next(
            { ...response, data: { rooms: [response.data.room, ...this.dataSubject.value.data.rooms]} }
          );
          //document.getElementById('closeModel').click();
          this.closeModal();
          this.isLoading.next(false);
          roomForm.resetForm();
          return { dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }
        }),
        startWith({ dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }),
        catchError((error: string) => {
          this.isLoading.next(false);
          return of({ dataState: DataState.ERROR_STATE, error  })
        })
      );
    }

    searchRooms(searchTerm: string): void {
      this.appState$ = this.roomService.search$(searchTerm, this.dataSubject.value)
        .pipe(
          map(response => {
            return { dataState: DataState.LOADED_STATE, appData: response }
          }),
          startWith({ dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }),
          catchError((error: string) => {
            return of({ dataState: DataState.ERROR_STATE, error  })
          })
        )
    }

    openModal() {
        const modelDiv = document.getElementById('addRoomModel');
        if(modelDiv != null){
          modelDiv.style.display = 'block';
        }
        //this.modalVisible = true;
      }
      closeModal() {
        const modelDiv = document.getElementById('addRoomModel');
        if(modelDiv != null){
          modelDiv.style.display = 'none';
        }
        //this.modalVisible = false;
      }

}