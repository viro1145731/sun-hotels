import { DataState } from "../enum/data-state.enum";

export interface AppState<T> {
    dataState?: DataState;
    dataStateHotel?: DataState;
    appData?: T;
    error?: string;
}