import { Hotel } from "./hotel";
import { Room } from "./room";

export interface CustomResponse {
    timeStamp: Date;
    stausCode: number;
    status: string;
    reason: string;
    message: string;
    developerMessage: string;
    data: { hotels?: Hotel[], hotel?: Hotel, rooms?: Room[], room?:Room};
}