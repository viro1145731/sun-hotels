export interface Hotel {
    id: number;
    hotelName: string;
    location: string;
    startDate: Date;
    endDate: Date;
    status: Boolean;
}