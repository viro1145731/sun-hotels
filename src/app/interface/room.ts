import { Hotel } from "./hotel";

export interface Room {
    id: number;
    roomType: string;
    roomNumber: number;
    maxAdult: number;
    price: number;
    hotel: Hotel; // Reference to the Hotel ID this room belongs to
  }