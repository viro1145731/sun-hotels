import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CustomResponse } from "../interface/custom-response";
import { Observable, Subscriber, subscribeOn, throwError } from "rxjs";
import { tap, catchError } from "rxjs";
import { Hotel } from "../interface/hotel";


//CurrentDate
let today = new Date();
//var today = new Date(d.getFullYear(), d.getMonth(), d.getDate());
// (YYYY, MM, DD) 

@Injectable({
    providedIn: 'root'
})



export class HotelService {

    private readonly apiUrl = 'http://localhost:8080';


    constructor(private http: HttpClient) { }

    hotels$ = <Observable<CustomResponse>>
    this.http.get<CustomResponse>(`${this.apiUrl}/hotel/list`)
    .pipe(
        tap(console.log),
        catchError(this.handleError)
    );

    hotel$ = (hotelId: number) =><Observable<CustomResponse>>
    this.http.get<CustomResponse>(`${this.apiUrl}/hotel/get/${hotelId}`)
    .pipe(
        tap(console.log),
        catchError(this.handleError)
    );

    save$ = (hotel: Hotel) => <Observable<CustomResponse>>
    this.http.post<CustomResponse>(`${this.apiUrl}/hotel/save`,hotel)
    .pipe(
        tap(console.log),
        catchError(this.handleError)
    );

    // filter$ = (status: string, response: CustomResponse) => <Observable<CustomResponse>>
    // new Observable<CustomResponse>(
    //     subscriber => {
    //         console.log(response);
    //         subscriber.next(
    //             {
    //                 ...response,
    //                 message: status=='active' ? response.data.hotels
    //                 .filter(hotel => hotel.endDate.getDate() >= today.getDate()).length > 0 ? `Hotels filtered by active contracts` : `No Hotels are found`
    //                 : status=='inactive' ? response.data.hotels
    //                 .filter(hotel => hotel.endDate.getDate() < today.getDate()).length > 0 ? `Hotels filtered by inactive contracts` : `No Hotels are found` : `Error`,
    //                 data: status=='active' ? {hotels: response.data.hotels
    //                     .filter(hotel => hotel.endDate.getDate() >= today.getDate())}
    //                     : status=='inactive' ? {hotels: response.data.hotels
    //                         .filter(hotel => hotel.endDate.getDate() < today.getDate())}
    //                         : {hotels:response.data.hotels}
    //             }
    //         )
    //     }
    // )

    filter$ = (status: string, response: CustomResponse) => <Observable<CustomResponse>>
    new Observable<CustomResponse>(
        subscriber => {
            //console.log(response);
            let message: string;
            let data: { hotels: Hotel[] };

            if (status === 'active') {
                const filteredHotels = response.data.hotels.filter(hotel => new Date(hotel.endDate) >= today && new Date(hotel.startDate) <= today);
                message = filteredHotels.length > 0 ? `Hotels filtered by active contracts` : `No Hotels are found in inactive status`;
                data = { hotels: filteredHotels };
                //  console.log(today.getTime)
            } else if (status === 'inactive') {
                const filteredHotels = response.data.hotels.filter(hotel => new Date(hotel.endDate) < today || new Date(hotel.startDate) > today);
                message = filteredHotels.length > 0 ? `Hotels filtered by inactive contracts` : `No Hotels are found in inactive status`;
                data = { hotels: filteredHotels };
            } else {
                message = 'Error';
                data = { hotels: response.data.hotels };
            }

            subscriber.next({
                ...response,
                message: message,
                data: data
            });

            subscriber.complete();
        }
    )
    .pipe(
        tap(console.log),
        catchError(this.handleError)
    );
    search$ = (status: string, response: CustomResponse) => <Observable<CustomResponse>>
    new Observable<CustomResponse>(
        subscriber => {
            //console.log(response);
            let message: string;
            let data: { hotels: Hotel[] };

            if (status) {
                const filteredHotels = response.data.hotels.filter(hotel => hotel.hotelName.toLowerCase().includes(status.toLowerCase()));
                message = filteredHotels.length > 0 ? `Hotels searched by ${status}` : `No Hotels are found`;
                data = { hotels: filteredHotels };
                //  console.log(today.getTime)
            } else {
                const filteredHotels = response.data.hotels;
                message = `Hotels`;
                data = { hotels: filteredHotels };
            } 

            subscriber.next({
                ...response,
                message: message,
                data: data
            });

            subscriber.complete();
        }
    )
    .pipe(
        tap(console.log),
        catchError(this.handleError)
    );

    delete$ = (hotelId: number) => <Observable<CustomResponse>>
    this.http.delete<CustomResponse>(`${this.apiUrl}/hotel/delete/${hotelId}`)
    .pipe(
        tap(console.log),
        catchError(this.handleError)
    );
    

    handleError(error: HttpErrorResponse): Observable<never> {
        console.log(error);
        return throwError (`An error occurred - Error code: ${error.status}`);
    }
}