import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, catchError, tap, throwError } from "rxjs";
import { CustomResponse } from "../interface/custom-response";
import { Room } from "../interface/room";

@Injectable({
    providedIn: 'root'
})

export class RoomService {

    private readonly apiUrl = 'http://localhost:8080';
    constructor(private http: HttpClient) {}

    rooms$ = (hotelId: number) => <Observable<CustomResponse>>
    this.http.get<CustomResponse>(`${this.apiUrl}/room/list/${hotelId}`)
    .pipe(
        tap(console.log),
        catchError(this.handleError)
    )

    delete$ = (roomId: number) => <Observable<CustomResponse>>
    this.http.delete<CustomResponse>(`${this.apiUrl}/room/delete/${roomId}`)
    .pipe(
        tap(console.log),
        catchError(this.handleError)
    );

    handleError(error: HttpErrorResponse): Observable<never> {
        console.log(error);
        return throwError (`An error occurred - Error code: ${error.status}`);
    }

    save$ = (room: Room,hotelId:number) => <Observable<CustomResponse>>
    this.http.post<CustomResponse>(`${this.apiUrl}/room/save/${hotelId}`,room)
    .pipe(
        tap(console.log),
        catchError(this.handleError)
    );

    search$ = (status: string, response: CustomResponse) => <Observable<CustomResponse>>
    new Observable<CustomResponse>(
        subscriber => {
            //console.log(response);
            let message: string;
            let data: { rooms: Room[] };

            if (status) {
                const filteredRooms = response.data.rooms.filter(room => room.roomType.toLowerCase().includes(status.toLowerCase()));
                message = filteredRooms.length > 0 ? `Rooms searched by ${status}` : `No Rooms are found`;
                data = { rooms: filteredRooms };
                //  console.log(today.getTime)
            } else {
                const filteredRooms = response.data.rooms;
                message = `Rooms`;
                data = { rooms: filteredRooms };
            } 

            subscriber.next({
                ...response,
                message: message,
                data: data
            });

            subscriber.complete();
        }
    )
    .pipe(
        tap(console.log),
        catchError(this.handleError)
    );

    


}